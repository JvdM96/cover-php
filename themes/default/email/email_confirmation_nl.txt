Subject: [Cover] Bevestig je e-mailadres
From: webcie@ai.rug.nl
Content-Type: text/html; charset=UTF-8

<!DOCTYPE html>
<html>
<body>
<p>Hoi $naam,</p>

<p>Je hebt aangegeven dat je je e-mailadres op de Coversite wil wijzigen naar $email|markup_format_text.</p>

<p>Klopt dat? Zo ja, dan moet je dit even bevestigen door naar de volgende pagina te gaan:<br>
<a href="$link|markup_format_attribute">$link|markup_format_text</a></p>

<p>Kind regards,</p>

<p>The AC/DCee</p>

<div itemscope itemtype="http://schema.org/EmailMessage">
  <div itemprop="potentialAction" itemscope itemtype="http://schema.org/ConfirmAction">
    <meta itemprop="name" content="Bevestig E-mailadres">
    <div itemprop="handler" itemscope itemtype="http://schema.org/HttpActionHandler">
      <link itemprop="url" href="$link|markup_format_text">
    </div>
  </div>
  <meta itemprop="description" content="Bevestig je e-mailadres">
</div>

</body>
</html>