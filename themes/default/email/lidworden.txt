Dear secretary,

The following membership registration was submitted today:

First name: $first_name
Family name prep: $family_name_preposition
Family name: $family_name
Gender: $gender

Address: $street_name
Postal code: $postal_code
Place: $place

Email: $email_address
Phone: $phone_number
Date of birth: $birth_date

Student number: $membership_student_number
Study: $membership_study_name $membership_study_phase
Year of enrollment: $membership_year_of_enrollment
IBAN: $iban
BIC: $bic
Authorization: $authorization

Mailing: $option_mailing

Cheers,

The AC/DCee
